# WIKI

https://bitbucket.org/tutorials/markdowndemo






Функция свайпа для touch устройств.
```
#!javascript

function swipeSlide( obj, callback ){
	var x = 0, x1 = 0, dx = 0, MinPath = 50;
	$(obj).on('touchstart', obj, function(e){
		x = e.pageX || e.originalEvent.targetTouches[0].pageX;
	});

	$('body').on('touchmove', obj, function(e){
		if ( x !== 0){
			x1 = e.pageX || e.originalEvent.targetTouches[0].pageX;
			dx = (( x1 - x ) > MinPath ) ? (1) : ( ( x1 - x ) < -MinPath ? (-1) : '0');
		}
	}).on('touchend', function(e){
		if ( x != 0 ){
			if ( callback )	callback(dx);
				x = 0;
			}
		});
	}

// Пример вызова функции

$('.element').each(function () {
	var $t = $(this);
	swipeSlide($t, function(x){
		if ( x < 0){
			$t.find('.control-prev').click();
		} else if ( x > 0){
			$t.find('.control-next').click();
		}
	})
});

```


