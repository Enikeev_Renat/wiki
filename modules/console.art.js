
(function () {
	console.art = function (name, style) {
		if ( ascii[name] == undefined ) {
			console.error('no art called «' + name + '»');
		} else {
			for (var i = 0; i < ascii[name].length; i++){
				console.log((typeof style == 'string' ? '%c' : '') + ascii[name][i], (typeof style == 'string' ? style : ''));
			}
		}
	};

	var ascii = {
		'skull': ['            _,.---,---.,_','        ,;~\'             \'~;,','      ,;                     ;,','     ;                         ; ','    ,\'                          \'','   ,;                           ;,','   ; ;      .           .      ; |','   | ;   ______       ______   ; |','   |  \'/~"     ~" . "~     "~\'   |','   |  ~  ,-~~~^~, | ,~^~~~-,  ~  |','    |   |        }:{        |   |','    |   l       / | \\       !   |','    .~  (__,.--" .^. "--.,__)  ~.','    |    ----;\' / | \           |','     \\__.       \\/^\\/       .__/','      V| \\                 / |V ','       | |T~\\___!___!___/~T| |','       | |`IIII_I_I_I_IIII\'| |','       |  \\,III I I I III,/  |','        \\   `~~~~~~~~~~\'    /','          \\   .       .   /','            \\.    ^    ./','              ^~~~^~~~^']
	}
})();




// 
console.art('skull', 'font-weight: bold; font-size: 16px; line-height: 0.5;');
