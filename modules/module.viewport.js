var module = module || {};

module.viewPort = {

	current: null,

	data: {
		'0': function(){
			module.viewPort.viewPort.current = 1;
			console.info('-----mobile-----');
		},
		'960': function(){
			module.viewPort.viewPort.current = 2;
			console.info('-----tabletHor-----');
		},
		'1300': function(){
			module.viewPort.viewPort.current = 3;
			console.info('-----desktop-----');
		},
		'1800': function(){
			module.viewPort.viewPort.current = 4;
			console.info('-----full HD-----');
		}
	},

	init: function(data){
		var points = data || module.viewPort.viewPort.data;
		if ( points ){
			points['Infinity'] = null;
			var sbw = scrollBarWidth(), curPoint = null;
			var ww = $(window).width() + sbw;
			checkCurrentViewport();
			$(window).on('resize', function(){
				ww = $(window).width() + sbw;
				checkCurrentViewport();
			});
		}

		function checkCurrentViewport(){
			var pp = 0, pc = null;
			$.each(points, function(point, callback){
				if ( point > ww ){
					if ( pp !== curPoint ) {
						curPoint = pp;
						pc();
					}
					return false;
				}
				pp = point; pc = callback;
			});
		}

		function scrollBarWidth(){
			var scrollDiv = document.createElement('div');
			scrollDiv.className = 'scroll_bar_measure';
			$(scrollDiv).css({
				width: '100px',
				height: '100px',
				overflow: 'scroll',
				position: 'absolute',
				top: '-9999px'
			});
			document.body.appendChild(scrollDiv);
			sbw = scrollDiv.offsetWidth - scrollDiv.clientWidth;
			document.body.removeChild(scrollDiv);
			return sbw;
		}
	}
};


var data = {
	'0': function(){
		console.info('-----mobile-----');
	},
	'960': function(){
		console.info('-----tabletHor-----');
	},
	'1300': function(){
		console.info('-----desktop-----');
	},
	'1800': function(){
		console.info('-----full HD-----');
	}
};

module.viewPort.init(data);